import React from 'react';
import Navbar from './components/Navbar';
import Sidebar from './components/Sidebar';
import Breadcrumb from './components/Breadcrumb/';
import PasswordChange from './components/PasswordChange';
import Footer from './components/Footer';
import './App.scss';

function App() {
  return (
    <div>
      <Navbar />
      <Sidebar />
      <div className='contents'>
        <Breadcrumb />
        <PasswordChange />
        <Footer />
      </div>
    </div>
  );
}

export default App;
