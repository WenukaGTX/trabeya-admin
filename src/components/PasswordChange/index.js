import React, { Component } from 'react';
import FormInput from '../FormInput';
import Button from '../Button';
import Error from '../Error';
import userSketch from '../../assets/user-sketch.png';
import './passwordchange.scss';

class PasswordChange extends Component {
    render() {
        return (
            <div className="flex justify-center align-center frow-1 password-reset">
                <div className='ui-card'>
                    <div className='form-head'>
                        <img className='user-sketch' src={userSketch} />
                        <h2>Inside The App</h2>
                        <p>Enter the new password then<br/> your password will change! Don't forget again</p>
                    </div>
                    <form>
                        <FormInput inputType='password' iconTitle='padlock' placeholder='Old password' />
                        <FormInput inputType='password' iconTitle='padlock' placeholder='New password' />
                        <FormInput inputType='password' iconTitle='padlock' placeholder='Confirm password' />
                        <Error errorType='warning' iconTitle='padlock' errorText='Please enter a valid name' />
                        <Button extraClass='width' btnType='default' btnText='Set Password' />
                    </form>
                </div>
            </div>
        )
    }
}

export default PasswordChange;