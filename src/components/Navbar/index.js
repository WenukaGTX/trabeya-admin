import React, { Component } from 'react';
import Search from '../Search';
import Dropdown from '../Dropdown';
import Icon from '../Icon';
import logo from '../../assets/trabeya-logo.svg';
import './navbar.scss';

class Navbar extends Component {
    sidebarOpen() {
        console.log('open')
        var sidebar = document.getElementsByClassName('sidebar')[0]
        sidebar.classList.toggle('open');
    }

    render() {
        return (
            <nav className="navbar">
                <div className='narbar-left' >
                    <img src={logo} className="navbar-logo" alt="logo" onClick={this.sidebarOpen} />
                </div>
                <div className='narbar-center' >
                    <Search />
                    <div className='time-date' >
                        <span>02:36 PM</span>
                        <span>Today</span>
                        <span>17 March, 2020</span>
                    </div>
                </div>
                <div className='narbar-right' >
                    <ul className='menu'>
                        <li className='unread blue'>
                            <button className='button'><Icon iconTitle='envelope' /></button>
                        </li>
                        <li className='unread orange'>
                            <button className='button'><Icon iconTitle='bell' /></button>
                        </li>
                        <li>
                            <Dropdown />
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}

export default Navbar;