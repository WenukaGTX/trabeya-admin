import React, { Component } from 'react';
import UserAvatar from '../../assets/user-72353-avatar.png';
import './dropdown.scss';

class Dropdown extends Component {
    render() {
        return (
            <div className="dropdown">
                <div className='dropdown-button'>
                    <span className='user-name'>Allie Sheman</span>
                    <img className='user-avatar' src={UserAvatar} />
                </div>
                <div className='dropdown-menu'>
                    <a href=''>Profile</a>
                    <a href=''>Settings</a>
                    <a href=''>Logout</a>
                </div>
            </div>
        )
    }
}

export default Dropdown;