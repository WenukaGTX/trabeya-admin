import React, { Component } from 'react';
import Icon from '../Icon';
import './form-input.scss';

class FormInput extends Component {
    render() {
        return (
            <div className="form-input-box transparent large">
                {(this.props.iconTitle) ? <Icon extraClass='pre-icon' iconTitle={this.props.iconTitle} /> : ''}
                <input type={this.props.inputType} className='form-input' placeholder={this.props.placeholder} />
            </div>
        )
    }
}

export default FormInput;