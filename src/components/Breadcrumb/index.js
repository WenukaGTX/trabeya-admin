import React, { Component } from 'react';
import './breadcrumb.scss';

class Breadcrumb extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ol className='breadcrumb'>
                <li className='item'>Inside the App</li>
                <li className='item'>Dashboard</li>
                <li className='item'>Inside the App</li>
            </ol>
        )
    }
}

export default Breadcrumb;