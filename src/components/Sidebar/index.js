import React, { Component } from 'react';
import Icon from '../Icon';
import Button from '../Button';
import UserAvatar from '../../assets/user-72353-avatar.png';
import './sidebar.scss';

class Sidebar extends Component {
    render() {
        return (
            <nav className="sidebar open">
                <div className='user-head'>
                    <img className='user-avatar' src={UserAvatar} />
                    <p className='user-name'>Allie Sherman</p>
                    <p className='user-designation'>UI / UX Designer</p>
                </div>
                <ul className='menu vertical invert overflow-hidden'>
                    <li className='active'>
                        <button className='button'>
                            <Icon iconTitle='home' />
                            <span>Home</span>
                        </button>
                    </li>
                    <li>
                        <button className='button'>
                            <Icon iconTitle='interface' />
                            <span>Apps</span>
                        </button>
                    </li>
                    <li>
                        <button className='button'>
                            <Icon iconTitle='user' />
                            <span>Profile</span>
                        </button>
                    </li>
                    <li>
                        <button className='button'>
                            <Icon iconTitle='business' />
                            <span>Reports</span>
                        </button>
                    </li>
                    <li>
                        <button className='button'>
                            <Icon iconTitle='computer' />
                            <span>Inside the App</span>
                        </button>
                    </li>
                    <li>
                        <button className='button'>
                            <Icon iconTitle='technology' />
                            <span>NLQ</span>
                        </button>
                    </li>
                </ul>
                <div className='mail-box'>
                    <Button btnType='icon-button' iconTitle='envelope-1' />
                    <span className='count'>2</span>
                </div>
            </nav>
        )
    }
}

export default Sidebar;