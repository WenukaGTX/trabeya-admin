import React, { Component } from 'react';
import Icon from '../Icon';
import './search.scss';

class Search extends Component {
    render() {
        return (
            <div className="search">
                <button className='button positive search-button' ><Icon iconTitle='loupe' /></button>
                <input className='form-input search-input' type='text' placeholder='Search' />
            </div>
        )
    }
}

export default Search;