import React, { Component } from 'react';
import Icon from '../Icon';
import './error.scss';

class Error extends Component {
    render() {
        return (
            <div className={`error ${this.props.errorType}`}>
                {(this.props.iconTitle) ? <Icon extraClass='error-icon' iconTitle={this.props.iconTitle} /> : ''}
                {this.props.errorText}
            </div>
        )
    }
}

export default Error;