import React, { Component } from 'react';
import './icon.scss';

class Icon extends Component {
    render() {
        return (
            <svg className={`icon ${this.props.extraClass ? this.props.extraClass : '' }`}><use xlinkHref={`#` + this.props.iconTitle} /></svg>
        )
    }
}

export default Icon;