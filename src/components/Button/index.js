import React, { Component } from 'react';
import Icon from '../Icon';
import './button.scss';

class Button extends Component {
    render() {
        return (
            <button className={`button ${this.props.btnType}`}>
                {(this.props.iconTitle) ? <Icon extraClass='btn-icon' iconTitle={this.props.iconTitle} /> : ''}
                {this.props.btnText}
            </button>
        )
    }
}

export default Button;