import React, { Component } from 'react';
import './footer.scss';

class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <nav className="footer">Designed & powered by Trabeya</nav>
        )
    }
}

export default Footer;